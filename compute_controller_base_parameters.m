function param = compute_controller_base_parameters
    % load truck parameters
    load('system/parameters_building');
    
    % Task 1: continuous time dynamics in state space form
    Ac = [ 
            1/building.m_VC * [-building.a_F1_VC - building.a_F2_VC - building.a_Env_VC, ...
                                building.a_F1_VC, ...
                                building.a_F2_VC];
            1/building.m_F1 * [ building.a_F1_VC,...
                               -building.a_F1_VC - building.a_F2_F1,...
                                building.a_F2_F1];
            1/building.m_F2 * [ building.a_F2_VC,...
                                building.a_F2_F1,...
                               -building.a_F2_VC - building.a_F2_F1]
         ];
    Bc = [1/building.m_VC * [building.b_11, building.b_12, building.b_13];
          1/building.m_F1 * [building.b_21, building.b_22, building.b_23];
          1/building.m_F2 * [building.b_31, building.b_32, building.b_33];];
    Bdc = [1/building.m_VC, 0,               0,               building.a_Env_VC/building.m_VC;
           0,               1/building.m_F1, 0,               0;
           0,               0,               1/building.m_F2, 0];
    d = [building.d_VC; 
         building.d_F1; 
         building.d_F2; 
         building.T_Env];
    
    % Task 2: discretization
    Ts = 60;
    A = eye(3) + Ts * Ac;
    B = Ts*Bc;
    Bd = Ts*Bdc;
    
    % Task 3: set point computation
    b_ref = 0; %TODO
    C_ref = eye(3); %TODO
    T_sp = .5 * (building.StateConstraints(:, 1) + building.StateConstraints(:, 2));
    p_sp = Bc\(-Ac*T_sp-Bdc*d);
    
    % Task 4: constraints for delta formulation
    Pcons = building.InputConstraints;
    Tcons = building.StateConstraints;
    Ucons = Pcons - p_sp;
    Xcons = Tcons - T_sp;
    
    
    % put everything together
    param.A = A;
    param.B = B;
    param.Bd = Bd;
    param.d = d;
    param.b_ref = b_ref;
    param.C_ref = C_ref;
    param.T_sp = T_sp;
    param.p_sp = p_sp;
    param.Ucons = Ucons;
    param.Xcons = Xcons;
    param.Tcons = Tcons;
    param.Pcons = Pcons;

    param.A_aug = eye(7);
    param.A_aug(1:3, 1:3) = param.A;
    param.A_aug(1:3, 4:end) = param.Bd;

    param.B_aug = zeros(7, 3);
    param.B_aug(1:3, 1:3) = eye(3);

    param.C_aug = zeros(3, 7);
    param.C_aug(1:3, 1:3) = eye(3);
    param.Cd_aug = zeros(4, 7);
    param.Cd_aug(1:4, 4:end) = eye(4);

    param.D_aug = zeros(3, 3);
    
end
