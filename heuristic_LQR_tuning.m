% BRRIEF:
%   Template for tuning of Q and R matrices for LQR controller as described 
%   in task 6.
% INPUT:
%   n_samples:  Number of samples considered for the tuning.
%   T0:         Initial condition
%   T_sp:       Set point
%   scen:       Disturbance scenario
% OUTPUT:
%   Q, R: Describes stage cost of LQR controller (x^T Q x + u^T R u)

function [Q, R] = heuristic_LQR_tuning(n_samples, T0, T_sp, scen)

f = waitbar(0,'Initializing...');

figure(2); set(gcf, 'WindowStyle' ,'docked'); grid on; hold on
xlabel('Energy consumption [kWh]'); 
ylabel('Relative norm of steady state deviation');

dT_relative_best = Inf;
power_best = 0;

R = eye(3);
k = 15;
Q = R;

for index = 1:n_samples
    waitbar(index/n_samples, f, "Heuristic Tuning, " + index + "/"+ n_samples);
    Q_test = diag([randi([1, 10e6]), randi([1, 10e6]), randi([1, 10e6])]);
    
    clear controller_lqr
    
    [T, p,~,~, T_v, p_v]= simulate_building(T0, @controller_lqr, Q_test, R, scen, 0);
    dT_relative = norm(T_sp-T(k))/norm(T_sp-T0);
    power_sum = sum(abs(p), 'all')/1000/60;
    
    color = 'green';
    if T_v
        color = 'red';
    elseif p_v
        color = 'blue';
    end
    
    if ~T_v && ~p_v && dT_relative < dT_relative_best && power_sum < 16
        Q = Q_test;
        dT_relative_best = dT_relative;
        power_best = power_sum;
    end
    
    scatter(power_sum, dT_relative, 'filled', color)
    
end

scatter(power_best, dT_relative_best, 'x', 'magenta','LineWidth',2)
close(f)
end
