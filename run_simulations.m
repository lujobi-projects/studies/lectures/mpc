%% Init
clear all
close all
addpath(genpath(cd));
rng(1234)
load('system/parameters_scenarios.mat')
% dT0_exmaple = ...
% T0_example = ...

building = compute_controller_base_parameters;
T_sp = building.T_sp;


%% Example
%figure; set(gcf, 'WindowStyle' ,'docked');
% clear persisten variables of function controller_example
% clear controller_example
% execute simulation
% [~,~,~,~] = simulate_building(T0_example,@controller_example);

%% First fig
figure(1); set(gcf, 'WindowStyle' ,'docked');
% clear persisten variables of function controller_example
% clear controller_example
% execute simulation
[~,~,~,~] = simulate_building(building.T_sp);

%% Unconstrained optimal control
disp('Unconstraint optimal control');

% Uncontrolled system
figure(1); set(gcf, 'WindowStyle' ,'docked');
T0_1 = building.T_sp + [-2.25; 1.75;  0.75];
T0_2 = building.T_sp + [  1.5; 2.75; -0.25];

% Tuning of LQR on first initial condition
[Q,R] = heuristic_LQR_tuning(2000, T0_1, T_sp, scen1);

figure(3); set(gcf, 'WindowStyle' ,'docked');
[~,~,~,~] = simulate_building(T0_1, @controller_lqr, Q, R, scen1);
[~,~,~,~] = simulate_building(T0_2, @controller_lqr, Q, R, scen1);

pause;

%% InvSet test
[A_x, b_x] = compute_X_LQR(Q, R);

sol = Polyhedron(A_x, b_x);
figure(4); set(gcf, 'WindowStyle' ,'docked');
sol.plot();

%% From LQR to MPC
disp('First MPC'); 


figure(5); set(gcf, 'WindowStyle' ,'docked');
[~,~,J,~] = simulate_building(T0_1, @controller_mpc_1, Q, R, scen1)
[~,~,J,~] = simulate_building(T0_2, @controller_mpc_1, Q, R, scen1)


pause;


%% MPC with guarantees
disp('MPC with guarantees');

figure(6); set(gcf, 'WindowStyle' ,'docked');
[~,~,J,~] = simulate_building(T0_1, @controller_mpc_2, Q, R, scen1)
[~,~,J,~] = simulate_building(T0_2, @controller_mpc_2, Q, R, scen1)


figure(7); set(gcf, 'WindowStyle' ,'docked');
[~,~,J,~] = simulate_building(T0_1, @controller_mpc_3, Q, R, scen1)
[~,~,J,~] = simulate_building(T0_2, @controller_mpc_3, Q, R, scen1)

pause;


%% Soft-constrained MPC
disp('Soft-constrained MPC');

figure(8); set(gcf, 'WindowStyle' ,'docked');
[~,~,J,~] = simulate_building(T0_1, @controller_mpc_3, Q, R, scen2)
[~,~,J,~] = simulate_building(T0_1, @controller_mpc_4, Q, R, scen2)

pause;


%% Offset-free MPC
disp('Offset-free MPC');
param = compute_controller_base_parameters;
L = place(param.A_aug, param.B_aug, [-.25, -.35 -.45, -.55, -.65, -.75, -.85])

A_e = (param.A_aug + L * param.C_aug)
eig(A_e)

pause;

figure(9); set(gcf, 'WindowStyle' ,'docked');
[~,~,J,~] = simulate_building(T0_1, @controller_mpc_6, Q, R, scen2)
pause;


%% Comparison using forces
disp('MPC Implementation with FORCES Pro');
