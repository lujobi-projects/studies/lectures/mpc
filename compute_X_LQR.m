% BRRIEF:
%   Template for explicit invariant set computation. You MUST NOT change
%   the output.
% INPUT:
%   Q, R: State and input weighting matrix, dimension (3,3)
% OUTPUT:
%   A_x, b_x: Describes polytopic X_LQR = {x| A_x * x <= b_x}

function [A_x, b_x] = compute_X_LQR(Q, R)
    param = compute_controller_base_parameters;
    [K,~,~] = dlqr(param.A,param.B,Q,R);
    A_loc = zeros(6);
    A_loc(1:3, 1:3) = param.A - param.B*K;
    A_loc(4:6, 1:3) = -K;
    
    sys = LTISystem('A', A_loc);
    sys.x.min = [param.Xcons(:, 1); param.Ucons(:, 1)];
    sys.x.max = [param.Xcons(:, 2); param.Ucons(:, 2)];
    invSet = sys.invariantSet();
    
    sol = projection(invSet,1:3);
    
    A_x = sol.A;
    b_x = sol.b;
       
end