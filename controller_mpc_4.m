% BRIEF:
%   Controller function template. Input and output dimension MUST NOT be
%   modified.
% INPUT:
%   Q: State weighting matrix, dimension (3,3)
%   R: Input weighting matrix, dimension (3,3)
%   T: Measured system temperatures, dimension (3,1)
%   N: MPC horizon length, dimension (1,1)
% OUTPUT:
%   p: Heating and cooling power, dimension (3,1)

function p = controller_mpc_4(Q, R, T, N, ~)
persistent param yalmip_optimizer

% initialize controller, if not done already
if isempty(param)
    [param, yalmip_optimizer] = init(Q, R, N);
end

x = T-param.T_sp;

% evaluate control action by solving MPC problem
[u_mpc,errorcode] = yalmip_optimizer({x, .1});
if (errorcode ~= 0)
    warning('MPC4 infeasible');
end
p = u_mpc{1}(:, 1) + param.p_sp;
end

function [param, yalmip_optimizer] = init(Q, R, N)
% get basic controller parameters
param = compute_controller_base_parameters;
% get terminal cost
[~,S,~] = dlqr(param.A,param.B,Q,R);
% implement your MPC using Yalmip here

[A_x, b_x] = compute_X_LQR(Q, R);

nx = size(param.A,1);
nu = size(param.B,2);
U  = sdpvar(repmat(nu,1,N-1),ones(1,N-1),'full');
X  = sdpvar(repmat(nx,1,N),ones(1,N),'full');
ep = sdpvar(repmat(1,1,N),ones(1,N),'full');
T0 = sdpvar(nx,1,'full');
v  = sdpvar(1,1,'full');
objective = 0;
constraints = [(X{1}== T0)];
for k = 1:N-1
    constraints = [constraints, ...
        X{k+1} == param.A * X{k} + param.B * U{k}, ...
    ];
    if k > 1 
        constraints = [
            constraints, ...
            A_x * X{k} <= b_x + ep{k}, ...
            param.Xcons(:, 1) <= X{k}, X{k} <= param.Xcons(:, 2),  ...
            param.Ucons(:, 1) <= U{k}, U{k} <= param.Ucons(:, 2), ...
            ep{k} >= 0 ...
        ];
    end
    objective = objective + X{k}'*Q*X{k} + U{k}'*R*U{k} + v*abs(ep{k});
end
constraints = [constraints, A_x * X{N} <= b_x, param.Xcons(:, 1) <= X{N}, X{N} <= param.Xcons(:, 2), ep{N} >= 0];
objective = objective + X{N}'*S*X{N} + v*abs(ep{N});

parameters_in = {T0, v};
solutions_out = {[U{:}], [X{:}]};

ops = sdpsettings('verbose',0,'solver','quadprog');
yalmip_optimizer = optimizer(constraints,objective,ops,parameters_in,solutions_out);
end